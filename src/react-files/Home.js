import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import matchSorter from 'match-sorter';
import axios from 'axios';
import { aduceToken } from "../javascript/functiileMele.js";
import moment from "moment";

class Home extends React.Component {
	componentDidMount() {
		if (!this.props.transport.transports.length) {
			axios({
				url: `http://localhost:3001/transports/transports`,
				method: 'GET',
				headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' }
			}).then((transport) => {
				this.props.dispatch({ type: 'getTransports', transports: transport.data.transports });
			}).catch(error => {
				this.setState({ error: error.response.data.results })
			})
		}
	}

	filterMethodInput = (filter, rows, field) => {
		return matchSorter(rows, filter.value, { keys: [ field ] })
	};

	render() {
		var coloane = [ {
			Header: 'Adaugata de',
			accessor: 'name',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'name')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Punct de plecare',
			accessor: 'leave',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'leave')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Punct de sosire',
			accessor: 'arrive',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'arrive')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Mijloc de transport',
			accessor: 'transportType',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'transportType')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Ora si ziua plecarii',
			accessor: 'startedOn',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'startedOn')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ moment(props.value).format('DD MMM YYYY HH:mm') }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Durata calatoriei',
			accessor: 'duration',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'duration')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		} ];

		return (
			<div className="table-wrapper">
				<h1>Toate calatoriile</h1>
				<ReactTable
					className="table"
					defaultPageSize={ 20 }
					data={ this.props.transport.transports }
					columns={ coloane }
					filterable
					defaultFilterMethod={ (filter, row) => String(row[filter.id]) === filter.value }
					getTrProps={ (state, rowInfo) => {
						if (rowInfo && rowInfo.row) {
							return {
								onClick: (e) => {
									this.props.history.push(`/transport/${ rowInfo.original.id }`);
								}
							}
						} else {
							return {}
						}
					} }
				/>
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		transport: store.transport
	}
}


export default connect(mapStateToProps)(Home);