import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import matchSorter from 'match-sorter';
import axios from 'axios';
import { aduceToken } from "../javascript/functiileMele.js";
import moment from "moment";

class TransportsOfUser extends React.Component {
	componentDidMount() {
		if (!this.props.match.params.userId) {
			this.props.history.replace('/');
		}
		axios({
			url: `http://localhost:3001/transports/transports/${ this.props.match.params.userId }`,
			method: 'GET',
			headers: { 'Content-type': 'application/json' }
		}).then((transport) => {
			this.props.dispatch({ type: 'transportsByUser', transportsByUser: transport.data.transports });
		}).catch(error => {
			this.setState({ error: error.response.data.results })
		})
	}

	filterMethodInput = (filter, rows, field) => {
		return matchSorter(rows, filter.value, { keys: [ field ] })
	};

	componentWillUnmount() {
		this.props.dispatch({ type: 'resetTransportsByUser' })
	}

	render() {
		var coloane = [ {
			Header: 'Punct de plecare',
			accessor: 'leave',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'leave')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Punct de sosire',
			accessor: 'arrive',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'arrive')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Mijloc de transport',
			accessor: 'transportType',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'transportType')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Ora si ziua plecarii',
			accessor: 'startedOn',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'startedOn')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ moment(props.value).format('DD MMM YYYY HH:mm') }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Durata calatoriei',
			accessor: 'duration',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'duration')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		} ];

		return (
			<div className="table-wrapper">
				<h1>
					{ ( ( ( this.props.transport.transportsByUser || [] )[0] ) || {} ).name ?
						`Calatoriile adaugate de ${ ( ( ( this.props.transport.transportsByUser || [] )[0] ) || {} ).name }`
						:
						'Nicio calatorie adaugata de catre acest utilizator'
					}

				</h1>
				<ReactTable
					className="table"
					defaultPageSize={ 20 }
					data={ this.props.transport.transportsByUser }
					columns={ coloane }
					filterable
					defaultFilterMethod={ (filter, row) => String(row[filter.id]) === filter.value }
					getTrProps={ (state, rowInfo) => {
						if (rowInfo && rowInfo.row) {
							return {
								onClick: (e) => {
									this.props.history.push(`/transport/${ rowInfo.original.id }`);
								}
							}
						} else {
							return {}
						}
					} }
				/>
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		transport: store.transport,
		user: store.user
	}
}

export default connect(mapStateToProps)(TransportsOfUser);