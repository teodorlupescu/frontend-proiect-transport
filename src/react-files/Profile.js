import React from 'react';
import { connect } from 'react-redux';
import axios from "axios";
import { aduceToken, stergeJSON } from '../javascript/functiileMele.js'

class Profile extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			name: '',
			error: '',
			password: ''
		}
	}

	componentDidMount() {
		if (!aduceToken()) {
			this.props.history.replace('/login')
		}
		this.setState({
			email: this.props.user.user.email,
			name: this.props.user.user.name,
		})
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if (!prevProps.user.user.id && this.props.user.user.id && this.props.user.logat && !prevProps.user.logat) {
			this.setState({
				email: this.props.user.user.email,
				name: this.props.user.user.name,
			})
		}
	}

	updateValue = field => e => {
		this.setState({ [field]: e.target.value })
	};

	componentWillUnmount() {
		this.setState({
			email: '',
			password: '',
			error: ''
		})
	}

	saveName = () => {
		if (this.state.name) {
			axios({
				url: 'http://localhost:3001/users/name',
				method: 'PUT',
				headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' },
				data: { name: this.state.name, }
			}).then((user) => {
				this.props.dispatch({ type: 'saveName', name: this.state.name });
				this.setState({ error: 'Ti-ai schimbat numele cu succes' });
			}).catch((error) => {
				this.setState({ error: error.response.data.results });
			})
		}
	};

	savePassword = () => {
		if (this.state.password.length >= 7) {
			axios({
				url: 'http://localhost:3001/users/password',
				method: 'PUT',
				headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' },
				data: { password: this.state.password, }
			}).then((user) => {
				this.setState({ error: 'Ati schimbat parola cu succes' });
			}).catch((error) => {
				this.setState({ error: error.response.data.results });
			})
		}
	};

	deactivateAccount = () => {
		axios({
			url: 'http://localhost:3001/users/deactivate',
			method: 'DELETE',
			headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' }
		}).then(() => {
			this.props.dispatch({ type: 'deactivateAccount' });
			this.setState({ error: 'Success' });
			stergeJSON();
			this.props.history.replace('/')
		}).catch((error) => {
			this.setState({ error: error.response.data.results })
		})
	};

	render() {
		return (
			<div>
				<h3>Profil</h3>

				<div className="form-group">
					<label>Schimba numele</label>
					<input
						type="text"
						name='name'
						className="form-control"
						placeholder="Schimba numele"
						value={ this.state.name }
						onChange={ this.updateValue('name') }/>
				</div>

				<button className="btn btn-primary btn-block" onClick={ this.saveName }>Salveaza numele</button>

				<br/>
				<hr/>
				<br/>

				<div className="form-group">
					<label>Schimba parola</label>
					<input
						type="password"
						name='password'
						className="form-control"
						placeholder="Schimba parola"
						value={ this.state.password }
						onChange={ this.updateValue('password') }/>
				</div>

				<button className="btn btn-primary btn-block" onClick={ this.savePassword }>Salveaza parola</button>

				{ this.state.error }


				<br/>
				<hr/>
				<br/>
				<button className="btn btn-danger btn-block" onClick={ this.deactivateAccount }>Dezactiveaza cont
				</button>
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.user
	}
}

export default connect(mapStateToProps)(Profile);