import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Login from './Login.js';
import Register from './Register.js';
import { salveazaT, stergeJSON } from "../javascript/functiileMele.js";
import { Switch, Link, Route, withRouter } from "react-router-dom";
import Home from "./Home.js";
import axios from "axios";
import { aduceToken } from '../javascript/functiileMele.js'
import Profile from "./Profile.js";
import ResetPassword from "./ResetPassword.js";
import NewTransport from "./NewTransport.js";
import ViewTransport from "./ViewTransport.js";
import TransportsOfUser from "./TransportsOfUser.js";
import MyTransports from "./MyTransports.js";
import EditTransport from "./EditTransport.js";

class Index extends PureComponent {
	componentDidMount() {
		axios({
			url: 'http://localhost:3001/users/autologin',
			method: 'GET',
			headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' },
		}).then((user) => {
			if (user.data.status !== 'notloggedin') {
				this.props.dispatch({ type: 'autologin', user: user.data });
				if (this.props.location.pathname === '/register' || this.props.location.pathname === "/login") {
					this.props.history.replace('/');
				}
			}
		}).catch((error) => {
			stergeJSON();
			this.props.history.replace('/login');
		})
	}

	logout = () => {
		this.props.dispatch({ type: 'resetareuser' });
		stergeJSON();
		this.props.history.push('/');
	};

	render() {
		return (
			<div
				className="App"
				style={ { width: "100vw", minHeight: '100vh' } }>
				<nav className="navbar navbar-expand-lg navbar-light fixed-top">
					<div className="container">
						<Link className="navbar-brand" to={ "/" }>Feedback Transport</Link>
						<div className="collapse navbar-collapse" id="navbarTogglerDemo02">
							{
								!this.props.user.logat ?
									<ul className="navbar-nav ml-auto">
										<li className="nav-item">
											<Link className="nav-link" to={ "/login" }>Autentificare</Link>
										</li>
										<li className="nav-item">
											<Link className="nav-link" to={ "/register" }>Inregistrare</Link>
										</li>
									</ul>
									:
									<div className="navbar-nav ml-auto">
										<Link className="nav-link">Salut, { this.props.user.user.name }</Link>
										<Link className="nav-link" to={ "/new-transport" }>Adauga o calatorie</Link>
										<Link className="nav-link" to={ "/my-transports" }>Calatoriile mele</Link>
										<Link className="nav-link" to={ "/profile" }>Profil</Link>
										<Link
											className="nav-link"
											to={ "/login" }
											onClick={ this.logout }>
											Logout
										</Link>
									</div>
							}

						</div>
					</div>
				</nav>

				{ this.props.location.pathname === '/register'
				|| this.props.location.pathname === '/login'
				|| this.props.location.pathname === '/profile'
				|| this.props.location.pathname === '/reset-password'
				|| this.props.location.pathname === '/new-transport'
				|| this.props.location.pathname === '/edit-transport'
					?
					<div className="auth-wrapper">
						<div className="auth-inner">
							<Switch>
								<Route path={ `/profile` } component={ Profile }/>
								<Route path={ `/register` } component={ Register }/>
								<Route path={ `/login` } component={ Login }/>
								<Route path={ `/reset-password` } component={ ResetPassword }/>
								<Route path={ `/new-transport` } component={ NewTransport }/>
								<Route path={ `/edit-transport` } component={ EditTransport }/>
							</Switch>
						</div>
					</div>
					:
					null
				}

				<Switch>
					<Route exact path={ `/transport/:transportId` } component={ ViewTransport }/>
					<Route exact path={ `/transports/:userId` } component={ TransportsOfUser }/>
					<Route exact path={ `/my-transports` } component={ MyTransports }/>
					<Route exact path={ `/` } component={ Home }/>
				</Switch>
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.user
	}
}

export default withRouter(connect(mapStateToProps)(Index));
