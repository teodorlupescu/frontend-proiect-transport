import React from 'react';
import { connect } from 'react-redux';
import axios from "axios";
import { aduceToken, salveazaT } from '../javascript/functiileMele.js'
import { Link } from "react-router-dom";

class Login extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: ''
		}
	}

	componentDidMount() {
		if (aduceToken()) {
			this.props.history.replace('/')
		}
	}

	updateValue = field => e => {
		this.setState({ [field]: e.target.value })
	};

	componentWillUnmount() {
		this.setState({
			email: '',
			password: '',
			error: ''
		})
	}

	login = () => {
		if (this.state.email && this.state.password.length >= 7) {
			axios({
				url: 'http://localhost:3001/users/login',
				method: 'POST',
				headers: { 'Content-type': 'application/json' },
				data: {
					email: this.state.email,
					password: this.state.password
				}
			}).then((user) => {
				salveazaT(user.data.jsonwtoken);
				this.props.dispatch({ type: 'login', user: user.data });
				this.props.history.replace('/')
			}).catch((error) => {
				this.setState({ error: error.response.data.results })
			})
		}
	};

	render() {
		return (
			<div>
				<h3>Logare</h3>

				<div className="form-group">
					<label>Adresa de mail</label>
					<input
						type="email"
						name='email'
						className="form-control"
						placeholder="Introduceti adresa de email"
						value={ this.state.email }
						onChange={ this.updateValue('email') }/>
				</div>

				<div className="form-group">
					<label>Parola</label>
					<input
						type="password"
						name='password'
						className="form-control"
						placeholder="Introduceti parola"
						value={ this.state.password }
						onChange={ this.updateValue('password') }/>
				</div>

				<button className="btn btn-primary btn-block" onClick={ this.login }>Intra</button>
				{ this.state.error }
				<p className="forgot-password text-right"><Link className="nav-link" to={ "/reset-password" }>Ati uitat parola?</Link></p>
			</div>
		)
	}
}

export default connect()(Login);