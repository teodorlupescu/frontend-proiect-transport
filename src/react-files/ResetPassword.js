import React from 'react';
import { connect } from 'react-redux';
import axios from "axios";
import { aduceToken, stergeJSON } from '../javascript/functiileMele.js'

class ResetPassword extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			name: '',
			error: '',
			password: '',
			securityAnswer: ''
		}
	}

	componentDidMount() {
		if (aduceToken()) {
			this.props.history.replace('/')
		}
		this.setState({
			email: this.props.user.user.email,
			name: this.props.user.user.name,
		})
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if (!prevProps.user.user.id && this.props.user.user.id && this.props.user.logat && !prevProps.user.logat) {
			this.setState({
				email: this.props.user.user.email,
				name: this.props.user.user.name,
			})
		}
	}

	updateValue = field => e => {
		this.setState({ [field]: e.target.value })
	};

	componentWillUnmount() {
		this.setState({
			email: '',
			password: '',
			error: '',
			securityAnswer: ''
		})
	}

	resetPassword = () => {
		if (this.state.password.length >= 7 && this.state.securityAnswer) {
			axios({
				url: 'http://localhost:3001/users/reset-password',
				method: 'PUT',
				headers: { 'Content-type': 'application/json' },
				data: {
					password: this.state.password,
					email: this.state.email,
					securityAnswer: this.state.securityAnswer
				}
			}).then(() => {
				this.setState({ error: 'Ati schimbat parola cu succes' });
				this.props.history.replace('/login');
			}).catch((error) => {
				console.log(error)
				this.setState({ error: error.response.data.results });
			})
		}
	};

	render() {
		return (
			<div>
				<h3>Resetare parola</h3>

				<div className="form-group">
					<label>Adresa de mail</label>
					<input
						type="email"
						name='email'
						className="form-control"
						placeholder="Introduceti adresa de email"
						value={ this.state.email }
						onChange={ this.updateValue('email') }/>
				</div>

				<div className="form-group">
					<label>Parola noua</label>
					<input
						type="password"
						name='password'
						className="form-control"
						placeholder="Parola noua"
						value={ this.state.password }
						onChange={ this.updateValue('password') }/>
				</div>

				<div className="form-group">
					<label>Care este melodia preferata?</label>
					<input
						type="password"
						name='password'
						className="form-control"
						placeholder="Raspuns necesar pentru resetarea parolei"
						value={ this.state.securityAnswer }
						onChange={ this.updateValue('securityAnswer') }/>
				</div>

				<button className="btn btn-primary btn-block" onClick={ this.resetPassword }>Schimba parola</button>

				{ this.state.error }
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.user
	}
}

export default connect(mapStateToProps)(ResetPassword);