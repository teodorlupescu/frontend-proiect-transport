import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import matchSorter from 'match-sorter';
import axios from 'axios';
import { aduceToken, stergeJSON } from "../javascript/functiileMele.js";
import moment from "moment";

class MyTransports extends React.Component {
	componentDidMount() {
		if (!aduceToken()) {
			this.props.history.replace('/login')
		}
		axios({
			url: `http://localhost:3001/transports/my-transports`,
			method: 'GET',
			headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' }
		}).then((transport) => {
			this.props.dispatch({ type: 'myTransports', myTransports: transport.data.transports });
		}).catch(error => {
			this.setState({ error: error.response.data.results })
		})
	}

	filterMethodInput = (filter, rows, field) => {
		return matchSorter(rows, filter.value, { keys: [ field ] })
	};

	deleteTransport = (id) => {
		axios({
			url: `http://localhost:3001/transports/transport/${ id }`,
			method: 'DELETE',
			headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' }
		}).then(() => {
			this.props.dispatch({ type: 'deleteTransport', id: id });
		}).catch((error) => {
			this.setState({ error: error.response.data.results })
		})
	};

	render() {
		var coloane = [ {
			Header: 'Punct de plecare',
			accessor: 'leave',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'leave')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Punct de sosire',
			accessor: 'arrive',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'arrive')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Mijloc de transport',
			accessor: 'transportType',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'transportType')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Ora si ziua plecarii',
			accessor: 'startedOn',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'startedOn')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ moment(props.value).format('DD MMM YYYY HH:mm') }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Durata calatoriei',
			accessor: 'duration',
			filterMethod: (filter, rows) => {
				return this.filterMethodInput(filter, rows, 'duration')
			},
			filterAll: true,
			Cell: props => ( <div className="table-cell">{ props.value }</div> ),
			Filter: ({ filter, onChange }) => (
				<input
					className='table-filter-input'
					placeholder='Cauta...'
					value={ filter ? filter.value : '' }
					onChange={ event => onChange(event.target.value) }
				/>
			)
		}, {
			Header: 'Actiuni',
			accessor: 'id',
			filterable: false,
			Cell: props => (
				<div className="table-cell">
					<button className="btn btn-primary btn-block"
							onClick={ () => {
								this.props.history.push(`/transport/${ props.value }`);
							} }>
						View
					</button>
					<button className="btn btn-warning"
							style={ { width: 'calc(50% - 10px)', margin: 5 } }
							onClick={ () => {
								this.props.dispatch({ type: 'setEditingTransport', editingTransport: props.original });
								this.props.history.push(`/edit-transport`);
							} }>
						Edit
					</button>
					<button className="btn btn-danger" style={ { width: 'calc(50% - 10px)', margin: 5 } }
							onClick={ () => {
								this.deleteTransport(props.value)
							} }>
						Delete
					</button>
				</div>
			)
		} ];

		return (
			<div className="table-wrapper">
				<h1>Calatoriile mele</h1>
				<ReactTable
					className="table"
					defaultPageSize={ 20 }
					data={ this.props.transport.myTransports }
					columns={ coloane }
					filterable
					defaultFilterMethod={ (filter, row) => String(row[filter.id]) === filter.value }
				/>
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		transport: store.transport,
		user: store.user
	}
}

export default connect(mapStateToProps)(MyTransports);