import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { aduceToken } from '../javascript/functiileMele.js'
import axios from 'axios';

class NewTransport extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			leave: '',
			arrive: '',
			transportType: 'Metrou',
			startedOn: '',
			duration: '',
			busyFactor: '',
			observations: '',
			satisfaction: '',
		}
	}

	componentDidMount() {
		if (!aduceToken()) {
			this.props.history.replace('/login')
		}
	}

	updateValue = field => e => {
		this.setState({ [field]: e.target.value })
	};

	saveTransport = () => {
		axios({
			url: 'http://localhost:3001/transports/transport',
			method: 'POST',
			headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' },
			data: {
				leave: this.state.leave,
				arrive: this.state.arrive,
				transportType: this.state.transportType,
				startedOn: this.state.startedOn,
				duration: this.state.duration,
				busyFactor: this.state.busyFactor,
				observations: this.state.observations,
				satisfaction: this.state.satisfaction,
			}
		}).then((transport) => {
			this.props.dispatch({
				type: 'saveTransport',
				transport: transport.data.transport,
				user: transport.data.user
			});
			this.props.history.push(`/transport/${ transport.data.transport.id }`)
		}).catch(error => {
			this.setState({ error: error.response.data.results })
		})
	};

	componentWillUnmount() {
		this.setState({
			leave: '',
			arrive: '',
			transportType: '',
			startedOn: '',
			duration: '',
			busyFactor: '',
			observations: '',
			satisfaction: '',
		})
	}

	render() {
		return (
			<div>
				<h3>Adauga o calatorie</h3>

				<div className="form-group">
					<label>Punct de plecare</label>
					<input
						type="text"
						className="form-control"
						placeholder="ex: Piata Unirii"
						value={ this.state.leave }
						onChange={ this.updateValue('leave') }/>
				</div>

				<div className="form-group">
					<label>Punct de sosire</label>
					<input
						type="text"
						className="form-control"
						placeholder="ex: Piata Victoriei"
						value={ this.state.arrive }
						onChange={ this.updateValue('arrive') }/>
				</div>

				<div className="form-group">
					<label>Mijloc de transport</label>
					<select
						type="text"
						className="form-control"
						placeholder="ex: autobuz, metrou etc"
						value={ this.state.transportType }
						onChange={ this.updateValue('transportType') }>
						<option value="Metrou">Metrou</option>
						<option value="Autobuz">Autobuz</option>
						<option value="Tramvai">Tramvai</option>
					</select>
				</div>

				<div className="form-group">
					<label>Ziua si ora plecarii</label>
					<input
						type="datetime-local"
						className="form-control"
						placeholder="ex: 06.01.2020 13:17"
						value={ this.state.startedOn }
						onChange={ this.updateValue('startedOn') }/>
				</div>

				<div className="form-group">
					<label>Durata calatoriei</label>
					<input
						type="text"
						className="form-control"
						placeholder="ex: 20 min"
						value={ this.state.duration }
						onChange={ this.updateValue('duration') }/>
				</div>

				<div className="form-group">
					<label>Nivel de aglomeratie</label>
					<input
						type="number"
						className="form-control"
						min={ 1 }
						max={ 10 }
						placeholder="Pe o scara de la 1 la 10"
						value={ this.state.busyFactor }
						onChange={ this.updateValue('busyFactor') }/>
				</div>

				<div className="form-group">
					<label>Observatii</label>
					<textarea
						type="text"
						className="form-control"
						placeholder="ex: In masina a fost foarte cald si nu s-a dat drumul la aerul conditionat"
						value={ this.state.observations }
						onChange={ this.updateValue('observations') }/>
				</div>

				<div className="form-group">
					<label>Nivel de satisfactie</label>
					<input
						type="number"
						className="form-control"
						min={ 1 }
						max={ 10 }
						placeholder="Pe o scara de la 1 la 10"
						value={ this.state.satisfaction }
						onChange={ this.updateValue('satisfaction') }/>
				</div>


				<button className="btn btn-primary btn-block" onClick={ this.saveTransport }>Salveaza</button>
				{ this.state.error }
			</div>
		)
	}
}

export default connect()(NewTransport);