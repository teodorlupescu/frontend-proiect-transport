import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { aduceToken } from '../javascript/functiileMele.js'
import { FaRegLaugh, FaRegMeh, FaRegFrown } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import moment from 'moment';

class ViewTransport extends React.Component {
	componentDidMount() {
		if (!this.props.match.params.transportId) {
			this.history.replace('/')
		}
		if (!this.props.transport.transport.id || !this.props.transport.user.name) {
			axios({
				url: `http://localhost:3001/transports/transport/${ this.props.match.params.transportId }`,
				method: 'GET',
				headers: { 'Content-type': 'application/json', 'Authentication': aduceToken() || '' }
			}).then((transport) => {
				this.props.dispatch({
					type: 'getTransport',
					transport: transport.data.transport,
					user: transport.data.user
				});
			}).catch(error => {
				this.setState({ error: error.response.data.results })
			})
		}
	}

	componentWillUnmount() {
		this.props.dispatch({ type: 'resetTransport' })
	}

	render() {
		return (
			<div className="view-transport-wrapper">
				<div className="view-transport-same-line">
					<div className="view-transport width-35">
						<div className="view-transport-item">
						<span>
							Adaugat de: <Link to={ `/transports/${ this.props.transport.transport.addedBy }` }>
							<b>{ this.props.transport.user.name }</b>
						</Link>
						</span>
						</div>
					</div>
					<div className="width-65">
						<div className="view-transport">
							<div className="view-transport-item">
						<span>Punct de plecare: <a
							href={ `https://www.google.com/maps?q=${ this.props.transport.transport.leave }` }
							target="_blank"
							rel="noreferrer noopenner">{ this.props.transport.transport.leave }</a></span>
							</div>
							<div className="view-transport-item">
						<span>Punct de sosire: <a
							href={ `https://www.google.com/maps?q=${ this.props.transport.transport.arrive }` }
							target="_blank"
							rel="noreferrer noopenner">{ this.props.transport.transport.arrive }</a></span>
							</div>
						</div>
						<div className="view-transport">
							<div className="view-transport-item">
						<span>
							Mijloc de transport: { this.props.transport.transport.transportType }
						</span>
							</div>
							<div className="view-transport-item">
						<span>
							Ziua si ora plecarii: { moment(this.props.transport.transport.startedOn).format('DD MMM YYYY HH:mm') }
						</span>
							</div>
							<div className="view-transport-item">
						<span>
							Durata calatoriei: { this.props.transport.transport.duration }
						</span>
							</div>
							<div className="view-transport-item">
						<span>
							Nivel de aglomeratie: {
							this.props.transport.transport.busyFactor < 3 ? 'Mic' :
								this.props.transport.transport.busyFactor < 7 ? 'Mediu' :
									this.props.transport.transport.busyFactor <= 10 ? 'Mare' : null
						}
						</span>
							</div>
							<div className="view-transport-item">
						<span>
							Nivel de satisfactie: { this.props.transport.transport.satisfaction > 7 ?

							<FaRegLaugh color={ 'green' } size={ 24 } style={ { position: 'relative', top: -2 } }/>
							: this.props.transport.transport.satisfaction > 5 ?
								<FaRegMeh color={ 'orange' } size={ 24 } style={ { position: 'relative', top: -2 } }/>
								:
								<FaRegFrown color={ 'red' } size={ 24 } style={ { position: 'relative', top: -2 } }/>
						}
						</span>
							</div>
						</div>
					</div>
				</div>

				<div className="view-transport-item full-width">
						<span>
							Observatii: { this.props.transport.transport.observations }
						</span>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		transport: store.transport
	}
}

export default connect(mapStateToProps)(ViewTransport)