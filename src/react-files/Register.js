import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import axios from 'axios';
import { aduceToken } from "../javascript/functiileMele.js";

class Register extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: '',
			name: '',
			securityAnswer: '',
			error: ''
		}
	}

	componentDidMount() {
		if (aduceToken()) {
			this.props.history.replace('/')
		}
	}

	updateValue = field => e => {
		this.setState({ [field]: e.target.value })
	};

	componentWillUnmount() {
		this.setState({
			email: '',
			password: '',
			name: '',
			securityAnswer: ''
		})
	}

	register = () => {
		if (this.state.email && this.state.password.length >= 7) {
			axios({
				url: 'http://localhost:3001/users/register',
				method: 'POST',
				headers: { 'Content-type': 'application/json' },
				data: {
					email: this.state.email,
					password: this.state.password,
					name: this.state.name,
					securityAnswer: this.state.securityAnswer
				}
			}).then((user) => {
				this.props.dispatch({ type: 'register', user: user.data });
				this.props.history.push('/login')
			}).catch(error => {
				this.setState({ error: error.response.data.results })
			})
		}
	};

	render() {
		return (
			<div>
				<h3>Inregistrare</h3>

				<div className="form-group">
					<label>Numele dumneavoastra</label>
					<input
						type="text"
						name='name'
						className="form-control"
						placeholder="Introduceti numele"
						value={ this.state.name }
						onChange={ this.updateValue('name') }/>
				</div>

				<div className="form-group">
					<label>Adresa de mail</label>
					<input
						type="email"
						name='email'
						className="form-control"
						placeholder="Introduceti adresa de email"
						value={ this.state.email }
						onChange={ this.updateValue('email') }/>
				</div>

				<div className="form-group">
					<label>Parola</label>
					<input
						type="password"
						name='password'
						className="form-control"
						placeholder="Introduceti parola"
						value={ this.state.password }
						onChange={ this.updateValue('password') }/>
				</div>

				<div className="form-group">
					<label>Care este melodia preferata?</label>
					<input
						type="text"
						className="form-control"
						placeholder="Raspuns necesar pentru resetarea parolei"
						value={ this.state.securityAnswer }
						onChange={ this.updateValue('securityAnswer') }/>
				</div>

				<button className="btn btn-primary btn-block" onClick={ this.register }>Inregistreaza-ma</button>
				{ this.state.error }
				<p className="forgot-password text-right">
					Deja inregistrat? <Link className="nav-link" to={ "/login" }>Logare</Link></p>
			</div>
		)
	}
}

export default connect()(Register);