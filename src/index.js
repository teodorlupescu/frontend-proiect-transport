import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/index.css';
import './style/main.css';
import 'react-table/react-table.css';
import * as serviceWorker from './serviceWorker';
import RouteMain from "./react-files/RouteMain.js";
import { storeGenerate } from "./javascript/reduxstore.js";
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
var store = storeGenerate();

ReactDOM.render(
	<Provider
		store={ store }><BrowserRouter><RouteMain/></BrowserRouter></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
