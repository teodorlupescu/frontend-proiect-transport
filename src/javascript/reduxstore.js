import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer } from './reducermain.js';

export var storeGenerate = () => {
	return createStore(
		reducer(),
		composeWithDevTools(applyMiddleware(thunk),),
	);
}