import { combineReducers } from 'redux';
import userR from './user.js';
import transportR from './transport.js';

export var reducer = () => {
	return combineReducers({
		user: userR,
		transport: transportR,
	});
}