import _ from "lodash";

export default function userR(state = {
	user: {},
	logat: false
}, action) {
	let nstatNew = _.cloneDeep(state);
	switch (action.type) {
		case 'login': {
			nstatNew.user = action.user;
			nstatNew.logat = action.user.id ? true : false;
			return nstatNew;
		}
		case 'register': {
			nstatNew.user = action.user;
			return nstatNew;
		}
		case 'saveName': {
			nstatNew.user.name = action.name;
			return nstatNew;
		}
		case 'autologin': {
			nstatNew.user = action.user;
			nstatNew.logat = action.user.id ? true : false;
			return nstatNew;
		}
		case 'resetareuser': {
			nstatNew = {
				user: {},
				logat: false
			};
			return nstatNew;
		}
		case 'deactivateAccount': {
			nstatNew = {
				user: {},
				logat: false
			};
			return nstatNew;
		}
		default:
			return state;
	}
};