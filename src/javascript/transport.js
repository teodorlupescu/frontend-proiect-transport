import _ from "lodash";

export default function transportR(state = {
	transports: [],
	transport: {},
	user: {},
	transportsByUser: [],
	myTransports: [],
	editingTransport: {}
}, action) {
	let nstatNew = _.cloneDeep(state);
	switch (action.type) {
		case 'saveTransport': {
			nstatNew.transport = action.transport;
			nstatNew.user = action.user;
			return nstatNew;
		}
		case 'getTransport': {
			nstatNew.transport = action.transport;
			nstatNew.user = action.user;
			return nstatNew;
		}
		case 'getTransports': {
			nstatNew.transports = action.transports.map(item => {
				return {
					...item.transport,
					...item.user
				}
			});
			return nstatNew;
		}
		case 'transportsByUser': {
			nstatNew.transportsByUser = action.transportsByUser.map(item => {
				return {
					...item.transport,
					...item.user
				}
			});
			return nstatNew;
		}
		case 'myTransports': {
			nstatNew.myTransports = action.myTransports.map(item => {
				return {
					...item.transport,
					...item.user
				}
			});
			return nstatNew;
		}
		case 'resetTransportsByUser': {
			nstatNew.transportsByUser = [];
			return nstatNew;
		}
		case 'setEditingTransport': {
			nstatNew.editingTransport = action.editingTransport;
			return nstatNew;
		}
		case 'editTransport': {
			nstatNew.transport = {
				...nstatNew.transport,
				...action.transport
			};
			return nstatNew;
		}
		case 'deleteTransport': {
			nstatNew.transportsByUser = nstatNew.transportsByUser.filter(t => t.id !== action.id);
			nstatNew.myTransports = nstatNew.myTransports.filter(t => t.id !== action.id);
			nstatNew.transports = nstatNew.transports.filter(t => t.id !== action.id);
			return nstatNew;
		}
		case 'resetTransport': {
			nstatNew = {
				...state,
				transport: {},
				user: {}
			};
			return nstatNew;
		}
		default:
			return state;
	}
};